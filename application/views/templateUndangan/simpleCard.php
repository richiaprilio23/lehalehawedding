
<html lang="id-ID" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Marroon Card: Rama dan Shinta - Undangan Pernikahan Online</title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="shortcut icon" href=""/>
    <link rel="pingback" href="http://envita.net/xmlrpc.php">
	<link rel="stylesheet" href="http://envita.net/wp-content/themes/invitus%20gongkow/includes/countdown/css/styles.css" />
	<link rel="stylesheet" href="http://envita.net/wp-content/themes/invitus%20gongkow/includes/countdown/countdown/jquery.countdown.css" />
	<link href="http://envita.net/wp-content/themes/invitus%20gongkow/includes/css/undangan/marroon-card.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="http://envita.net/wp-content/themes/invitus%20gongkow/includes/css/swipebox.css" rel="stylesheet" type="text/css" media="screen" />
	<link rel="stylesheet" href="http://envita.net/wp-content/themes/invitus%20gongkow/css/bootstrap.min.css" />
	<script src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/js/jquery.min.js"></script>
	<script src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/js/jquery.swipebox.js"></script>
	<script src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/js/swipebox-config.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6J5-_NS1XuofKOPArJFF6bna9TWv8M48&callback=initMap"></script>
	<script src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/js/bookblock/modernizr.custom.js"></script>
	<script>
		$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			  var target = $(this.hash);
			  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			  if (target.length) {
				$('html,body').animate({
				  scrollTop: target.offset().top
				}, 600);
				return false;
			  }
			}
		  });
		});	
	</script>
	<script>
		$('body').on({
			'mousewheel': function(e) {
				if (e.target.id == 'el') return;
				e.preventDefault();
				e.stopPropagation();
			}
		})
	</script>	
	<script>
		$(document).ready(function(){
			$(".navigasi-wrp").hide();
			
			$(".menu-depan").click(function(){
				$(".navigasi-wrp").fadeIn();
			});
			$(".menu-isi").click(function(){
				$(".navigasi-wrp").fadeIn();
			});
			$(".menu-close").click(function(){
				$(".navigasi-wrp").fadeOut();
			});
			$(".nav-u").click(function(){
				$(".navigasi-wrp").fadeOut();
			});
		});
	</script> 
	<script>
		$(document).ready(function(){
			$(".resepsi").hide();
			$(".tbl-ak").addClass("tbl-aktif");
			
			$(".tbl-ak").click(function(){
				$(".akadnikah").fadeIn();
				$(".resepsi").hide();
				$(".tbl-ak").addClass("tbl-aktif");
				$(".tbl-res").removeClass("tbl-aktif");
			});
			$(".tbl-res").click(function(){
				$(".resepsi").fadeIn();
				$(".akadnikah").hide();
				$(".tbl-res").addClass("tbl-aktif");
				$(".tbl-ak").removeClass("tbl-aktif");
			});
		});
	</script> 
        <script>
	$(document).ready(function() {

	runslide();

	function runslide() {
		$('#nama-pengundang').fadeIn(1000).delay(2500).fadeOut(1000, function() {
			$('#nama-diundang').fadeIn(1000).delay(2500).fadeOut(1000, function() {
				runslide();
			});
		});
	}

	});
        </script>
	
</head>

<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69815216-3', 'auto');
  ga('send', 'pageview');

</script><div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.6&appId=462160143990173";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<audio autoplay loop id="myAudio">
  <source src="http://envita.net/backsound/nkc-love.mp3" type="audio/mpeg">
</audio> 
<style>
    #over-lay {
        position:fixed;
        width: 100vw;
        height: 100vh;
        background-color: rgba(0,0,0,0.75);
        border: none;
        color: white;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: block;
        font-size: 16px;
        margin: 0px;
        cursor: pointer;    
        z-index:9999;
        display: none;
    }
    .tapfp {
        text-align:center;
        font-size: 40px;
        line-height: 45px;
    }
@media screen and (max-width: 769px) {
    #over-lay {
        display: -webkit-flex; /* Safari */
        -webkit-align-items: center; /* Safari 7.0+ */
        display: flex;
        align-items: center;
    }    
}
</style>
<script>
$(document).ready(function(){
	$("#over-lay").click(function(){
			$("#over-lay").fadeOut(650);
		});
});
</script>	
<div onclick="playAudio()" type="button" id="over-lay">
    <div style="margin:0 auto;">
        <div class="tapfp">TAP UNTUK MELANJUTKAN</div>
    </div>
</div>
<script>
var x = document.getElementById("myAudio"); 

function playAudio() { 
    x.play(); 
} 

function pauseAudio() { 
    x.pause(); 
} 
</script>
			<div id="home" class="main-bodi">
				<div class="bg-depan">
						<div class="menu-depan-luar">
							<div class="menu-depan">
							</div>
						</div>
						<div class="bg-depan-dlm">
							<div class="konten-wrap kwhome">
								<img class="wedding-txt" src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/images/marroon-card/wedding-txt.png">
                                                                <div id="nama-pengundang">
								     <div class="tanggal-home">Minggu, 23 Juli 2017</div>
								     <div class="nama-home">Shinta & Rama</div>
                                                                </div>
                                                                <div id="nama-diundang">
								     <div class="tanggal-home">Kepada Yang Terhormat:</div>
								     <div class="nama-home">Nabila Astarina</div>
                                                                </div>


							</div>
						</div>
						<div class="kondon-luar">
							<div id="countdown"></div>

							<p id="note"></p>
							
							<!-- JavaScript includes -->
							<script src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/countdown/countdown/jquery.countdown.js"></script>
							<script>
							$(function(){
						
								var note = $('#note'),
									condon = '2018 02 23 09 00';
									cdThn = condon.substr(0,4);
									cdBln = condon.substr(5,2);
									cdTgl = condon.substr(8,2);
									cdJam = condon.substr(11,2);
									cdMnt = condon.substr(14,2);
									
									ts = new Date(cdThn, cdBln, cdTgl, cdJam, cdMnt, 0),
									newYear = true;
								
								if((new Date()) > ts){
									// The new year is here! Count towards something else.
									// Notice the *1000 at the end - time must be in milliseconds
									ts = (new Date()).getTime();
									newYear = false;
								}
									
								$('#countdown').countdown({
									timestamp	: ts,
									callback	: function(days, hours, minutes, seconds){
										
										var message = "";
										
										message += days + " hari" + ( days==1 ? '':'' ) + ", ";
										message += hours + " jam" + ( hours==1 ? '':'' ) + ", ";
										message += minutes + " menit" + ( minutes==1 ? '':'' ) + " and ";
										message += seconds + " detik" + ( seconds==1 ? '':'' ) + " <br />";
										
										if(newYear){
											message += "menuju resepsi";
										}
										else {
											message += "resepsi telah dilaksanakan";
										}
										
										note.html(message);
									}
								});
								
							});
							</script>
						</div>
				</div>
			</div>
			<div id="acara" class="main-bodi main-bodi-isi">
				<div class="bg-isi">
						<div class="menu-isi-luar">
							<div class="menu-isi">
							</div>
						</div>
						<div class="bg-isi-dlm">
							<div class="konten-wrap">
								<h1 style="text-transform:uppercase;">AKAD NIKAH & RESEPSI</h1>
								<p>Maha suci Allah SWT yang telah menciptakan makhluknya berpasang-pasangan, perkenankanlah kami melaksanakan syariat agamamu mengikuti sunnah Rasul-mu, membentuk keluarga Sakinah Mawadah Warahmah.</p>
								<div class="col-wraper">
									<div class="col-lep">
									<h1>Dewi Shinta</h1>
									<p>Putri dari Bpk. Janaka dan Ibu. Dewi Tari <br>Jakarta Selatan</p>
									</div>
									<div class="col-cen"><p class="dengan">&</p></div>
									<div class="col-rig">
									<h1>Sri Rama</h1>
									<p>Putra dari Bpk. Dasarata dan Ibu. Kausalya <br>Jakarta Selatan</p>								
									</div>
								</div>
								<div class="col-wraper mrghok">
									<div class="tbl-akres-luar">
										<span class="tbl-ak">AKAD NIKAH</span>
										<span class="tbl-res">RESEPSI</span>
									</div>
									<div class="ctrhok akadnikah">
										<p>Minggu, 23 Juli 2017<br>
										Jam 09:00 s/d 11:00 WIB<br>
										di Kediaman mempelai wanita. Jl. Damai Raya 31 Petukangan, Jakarta Selatan</p>
									</div>							
									<div class="ctrhok resepsi">
										<p>Minggu, 23 Juli 2017<br>
										Jam 11:30 s/d 15:00 WIB<br>
										di Kediaman mempelai wanita. Jl. Damai Raya 31 Petukangan, Jakarta Selatan</p>
									</div>	
								</div>
							</div>
						</div>
				</div>
			</div>
						<div id="story" class="main-bodi main-bodi-isi">
				<div class="bg-isi">
						<div class="menu-isi-luar">
							<div class="menu-isi">
							</div>
						</div>
						<div class="bg-isi-dlm">
							<div class="konten-wrap">
								<h1 class="ourstory" style="text-transform:uppercase;">Kisah Cinta</h1>
								<p>Berawal dari waktu yang lama, kami mengisi hari bersama, suka duka, canda tawa, berlari dan terjatuh. Mimpi yang mempersatukan kami, cinta yang membuat kami saling mengerti.
Kadang kami membuat kalian semua marah, benci dan kecewa, tapi mengertilah, kami hanya dua insan yang sedang terlanda asmara, kami hanya belajar untuk mengutamakan pasangan hidup kami daripada kalian tanpa melupakan kalian dan tanpa mengurangi rasa cinta kami pada kalian. Kami tetaplah teman dan sahabat kalian.
Tidak ada manusia yang sempurna di dunia ini, tapi kami akan bersama untuk saling melengkapi merajut hidup yang sempurna. Sungguh indah cinta ini, cinta kami yang ber ujung dipernikahan akan menjadi bukti keseriusan kami dalam menata kehidupan dan masa depan.
Semua masa yang telah kami lewati kini akan berakhir, dan kami akan memulai masa yang baru berdua dan slalu bersama untuk selamanya hingga ujung waktu.
Semua ini tak lepas dari doa kedua orang tua dan kalian semua, terimakasih telah senantiasa menghantar kami hingga ke titik ini. Mohon doa restu..</p>
							</div>
						</div>
				</div>
			</div>
									<div id="galeri" class="main-bodi main-bodi-isi">
				<div class="bg-isi">
						<div class="menu-isi-luar">
							<div class="menu-isi">
							</div>
						</div>
						<div class="bg-isi-dlm">
							<div class="konten-wrap  konten-wrap-galeri">
								<h1>Galeri Prewedding</h1>
								<div class="galeri" style="text-transform:uppercase;">
									<div class="col-md-4 col-sm-4 col-xs-6 col-saya"><div class="row"><a href="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1-1.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1-1-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="contoh-prewed-1" srcset="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1-1-300x200.jpg 300w, http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1-1-600x401.jpg 600w, http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1-1-768x513.jpg 768w, http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1-1-1024x684.jpg 1024w, http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1-1-1170x781.jpg 1170w, http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1-1.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></a></div></div><div class="col-md-4 col-sm-4 col-xs-6 col-saya"><div class="row"><a href="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-2-1.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-2-1-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="contoh-prewed-2" /></a></div></div><div class="col-md-4 col-sm-4 col-xs-6 col-saya"><div class="row"><a href="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-3-1.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-3-1-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="contoh-prewed-3" /></a></div></div><div class="col-md-4 col-sm-4 col-xs-6 col-saya"><div class="row"><a href="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-4-1.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-4-1-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="contoh-prewed-4" srcset="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-4-1-300x200.jpg 300w, http://envita.net/wp-content/uploads/2017/02/contoh-prewed-4-1-600x399.jpg 600w, http://envita.net/wp-content/uploads/2017/02/contoh-prewed-4-1.jpg 640w" sizes="(max-width: 300px) 100vw, 300px" /></a></div></div><div class="col-md-4 col-sm-4 col-xs-6 col-saya"><div class="row"><a href="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-5-1.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-5-1-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="contoh-prewed-5" /></a></div></div><div class="col-md-4 col-sm-4 col-xs-6 col-saya"><div class="row"><a href="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-6-1.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-6-1-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="contoh-prewed-6" /></a></div></div>	
								</div>
							</div>
						</div>
				</div>
			</div>
						<div id="buku-tamu" class="main-bodi main-bodi-isi">
				<div class="bg-isi">
						<div class="menu-isi-luar">
							<div class="menu-isi">
							</div>
						</div>
						<div class="bg-isi-dlm">
							<div class="konten-wrap  konten-wrap-galeri">
								<h1 style="text-transform:uppercase;">Buku Tamu</h1>
								<div class="komen-fb">	
								<div class="fb-comments fesbuk" data-href="http://envita.net/undangan/marroon-card-rama-dan-shinta/" data-numposts="5"></div>
								</div>	
							</div>
						</div>
				</div>
			</div>
			<div id="denah-lokasi" class="main-bodi main-bodi-isi">
				<div class="bg-isi" style="padding:0;">
						<div class="menu-isi-luar" style="margin-top: -34px;">
							<div class="menu-isi">
							</div>
						</div>
						<div class="bg-isi-dlm" style="border:none;">
							<div id="google-map" style="width: 100%; height: 100%;"></div> 
					 
							<script type="text/javascript">
								  
					//              menentukan koordinat titik tengah peta
								  var myLatlng = new google.maps.LatLng(-6.248503, 106.754860);
					 
					//              pengaturan zoom dan titik tengah peta
								  var myOptions = {
									  zoom: 13,
									  center: myLatlng,
									  scrollwheel: false
								  };
								  
					//              menampilkan output pada element
								  var map = new google.maps.Map(document.getElementById("google-map"), myOptions);
								  
					//              menambahkan marker
								  var marker = new google.maps.Marker({
									   position: myLatlng,
									   map: map,
									   title:"RESEPSI: Kediaman mempelai wanita. Jl. Damai Raya 31 Petukangan, Jakarta Selatan",
									   icon: "http://envita.net/wp-content/themes/invitus%20gongkow/includes/images/marroon-card/map_pin.png"
								  });

									var contentinfo = 
										'<h3>RESEPSI</h3>'+
										'<p>Kediaman mempelai wanita. Jl. Damai Raya 31 Petukangan, Jakarta Selatan</p>';
									 
									var infowindow = new google.maps.InfoWindow({
										content: contentinfo
										});
									 
									google.maps.event.addListener(marker, 'mouseover', function() {
									  infowindow.open(map,marker);
									});	
									google.maps.event.addListener(marker, 'mouseout', function() {
									  infowindow.close(map,marker);
									});					
							</script>																					
						</div>
				</div>
			</div>
			<div class="navigasi-wrp">
				<div class="menu-close-wrp"><div class="menu-close"></div></div>
				<div class="navigasi-utama">
					<a class="nav-u" style="text-transform:uppercase;" href="#home">Home</a>
					<a class="nav-u" style="text-transform:uppercase;" href="#acara">Acara</a>
										<a class="nav-u" style="text-transform:uppercase;" href="#story">Kisah Cinta</a>
															<a class="nav-u" style="text-transform:uppercase;" href="#galeri">Galeri</a>
										<a class="nav-u" style="text-transform:uppercase;" href="#denah-lokasi">Denah Lokasi</a>
					<a class="nav-u" style="text-transform:uppercase;" href="#buku-tamu">Buku Tamu</a>
				</div>
			</div>
</body>
</html>

<!-- Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/

 Served from: envita.net @ 2019-01-23 14:32:29 by W3 Total Cache -->