
<!DOCTYPE html>
<html lang="id-ID" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Great Day: Rama dan Shinta - Undangan Pernikahan Online</title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href=""/>
<link rel="pingback" href="http://envita.net/xmlrpc.php">

<!-- Bootstrap CSS -->
<link href="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/css/bootstrap.css" rel="stylesheet">
<link href="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/css/bootstrap-theme.css" rel="stylesheet">
<!-- Font Awesome CSS -->
<link href="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/css/font-awesome.min.css" rel="stylesheet">
<!-- Flaticon CSS -->
<link href="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/css/flaticon/flaticon.css" rel="stylesheet">
<!-- Owl Carousel CSS -->
<link href="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/css/owl.carousel.css" rel="stylesheet">
<link href="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/css/owl.transitions.css" rel="stylesheet">
<!-- ColorBox CSS -->
<link href="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/css/colorbox.css" rel="stylesheet">
<!-- Theme CSS -->
<link href="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/css/style.css" rel="stylesheet">
<!-- Color CSS -->
<link href="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/css/warna.css" rel="stylesheet">
<link href="http://envita.net/wp-content/themes/invitus%20gongkow/includes/css/swipebox.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="shortcut icon" href="http://envita.net/wp-content/themes/invitus%20gongkow/img/IconEnvita.png">

<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
<script src="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/js/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			$('.akad').hide();
			$('.tbl-res').addClass('tbl-aktif');
			
			$('.tbl-akd').click(function(){
				$('.akad').fadeIn();
				$('.resepsi').hide();
				$('.tbl-akd').addClass('tbl-aktif');
				$('.tbl-res').removeClass('tbl-aktif');
			});
			$('.tbl-res').click(function(){
				$('.resepsi').fadeIn();
				$('.akad').hide();
				$('.tbl-res').addClass('tbl-aktif');
				$('.tbl-akd').removeClass('tbl-aktif');
			});
		});
	</script> 

</head>

<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69815216-3', 'auto');
  ga('send', 'pageview');

</script><div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.6&appId=462160143990173";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<audio autoplay loop id="myAudio">
  <source src="http://envita.net/backsound/brian-mcknight-marry-your-daughter.mp3" type="audio/mpeg">
</audio> 
<style>
    #over-lay {
        position:fixed;
        width: 100vw;
        height: 100vh;
        background-color: rgba(0,0,0,0.75);
        border: none;
        color: white;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: block;
        font-size: 16px;
        margin: 0px;
        cursor: pointer;    
        z-index:9999;
        display: none;
    }
    .tapfp {
        text-align:center;
        font-size: 40px;
        line-height: 45px;
    }
    .background-cover-depan {
        background-image: url(http://envita.net/wp-content/uploads/2017/03/Original-image-2048x1200-2-1.jpg);
    }
@media screen and (max-width: 769px) {
    #over-lay {
        display: -webkit-flex; /* Safari */
        -webkit-align-items: center; /* Safari 7.0+ */
        display: flex;
        align-items: center;
    }    
}
	.title5 {
		font-size:26px;
	}
	.title5-b {
		font:34px/40px Roboto Condensed;
		text-transform:uppercase;
	}	
@media (max-width:480px){
	.title1-hp {
		font-size:50px;
                line-height: 110%;
	}
	.title2-hp {
		font-size:28px;
                line-height: 110%;
	}
	.title5 {
		font-size:20px;
	}
	.title5-b {
		font-size:20px;
	}	
    .background-cover-depan {
        background-image: url(http://envita.net/images/greatday-background-cover.jpg);	
    }
}    
</style>
<script>
$(document).ready(function(){
	$("#over-lay").click(function(){
			$("#over-lay").fadeOut(650);
		});
});
</script>	
<div onclick="playAudio()" type="button" id="over-lay">
    <div style="margin:0 auto;">
        <div class="tapfp">TAP UNTUK MELANJUTKAN</div>
    </div>
</div>
<script>
var x = document.getElementById("myAudio"); 

function playAudio() { 
    x.play(); 
} 

function pauseAudio() { 
    x.pause(); 
} 
</script>
 	<!-- Header -->
    <header>
        <div class="header_plane anim"></div>
        
        <!-- Menu Button -->
        <a class="main_menu_btn">
            <span class="line line1"></span>
            <span class="line line2"></span>
            <span class="line line3"></span>
        </a>
        
        
        <!-- Submenu -->
        <div class="main_menu_block">
            <div class="menu_wrapper">
                <div class="sub_menu anim">
                    <ul>
                        <li><a href="#depan">Home</a></li>
						<li><a href="#married">Pernikahan Kami</a></li>
                           						
                        <li><a href="#our_story">Kisah Cinta</a></li>
                                                   <li><a href="#when_where">Acara</a></li>
								
                        <li><a href="#rsvp">Konfirmasi Kehadiran</a></li>
																			
                        <li><a href="#gallery">Galeri</a></li> 
							
                        <li><a href="#google-map">Denah Lokasi</a></li>                   
                    </ul>
                </div>
                <div class="sub_img anim"></div>
            </div>
        </div>
        <!-- Submenu End -->
        
		<!-- Social Buttons -->
        <div class="header_social">
			            <a href="http://twitter.com/EnvitaOfficial" target="_blank"><i class="fa fa-twitter"></i></a>
            	
						<a href="http://facebook.com/envita.net" target="_blank"><i class="fa fa-facebook"></i></a>
            	
						<a href="http://instagram.com/envita.official" target="_blank"><i class="fa fa-instagram"></i></a>
				
		</div>	
        
    </header>
    <!-- Header End -->

<div class="page">
 
    <!-- Intro -->
    <section id="depan" class="home_intro white_txt parallax2 background-cover-depan">        
    	<div class="home_txt" data-0="opacity:1" data-top-bottom="opacity:0">
            
            <!-- Intro Text -->
            
            <div class="title1 title1_2 txt_shadow">Shinta & Rama</div>
			<div class="bullet txt_shadow">Walimatul &#039;Ursy</div>
        </div>		
    	<div class="into_firefly"></div> 
		<div class="panah"><a href="#married" class="scroll-down"><i class="fa fa-angle-down"></i></a></div>	
    </section>
	<!-- Intro End -->
    
    <!-- Married -->
    <section class="married clearfix" id="married">
        <div class="container">
		
		
                <div class="im1 parallax"  data-bottom="left:20%" data-center="left:0" style="background-image: url(
				http://envita.net/wp-content/uploads/2017/03/beckh-1.jpg				)"><div class="im_arrows"></div></div>
                <div class="im2 parallax" data-bottom="right:20%" data-center="right:0" style="background-image: url(
				http://envita.net/wp-content/uploads/2017/03/deckh.jpg				)"><div class="im_arrows"></div></div>

                <div class="married_txt" data-bottom="opacity:0" data-center="opacity:1">
                    <h2><span>Kepada</span>   Nabila Astarina</h2>
                    <div class="married_container">
                    Resepsi akan diadakan pada<br>Minggu, 23 Juli 2017<br>
                    <b>Dewi Shinta & Sri Rama</b><br>
                    di Kediaman mempelai wanita. Jl. Damai Raya 31 Petukangan, Jakarta Selatan<br>
                   
                    <a href="#rsvp" class="btn go">KONFIRMASI SEKARANG</a>                    </div>
                    <div class="married_coundown"></div>
                    <div class="double_arrow"></div>
                </div>
        </div>
    </section>
    <!-- Married End -->

    
    <!-- Story -->
	
    <section class="our_story" id="our_story">
		<h2><span>Kisah</span>  Cinta</h2>
		
		<!-- Wrapper -->
		<div class="story_wrapper">
                
				                <div class="story_item">
                    <div class="story_plane">
                        <div class="story_back"></div>
                        <div class="story_txt">
                            <div class="story_title">
                                Awal Cerita                            </div>
							Berawal dari waktu yang lama, kami mengisi hari bersama, suka duka, canda tawa, berlari dan terjatuh. Mimpi yang mempersatukan kami, cinta yang membuat kami saling mengerti.                        </div> 
                    </div>
                </div>
								                <div class="story_item">
                    <div class="story_plane">
                        <div class="story_back"></div>
                        <div class="story_txt">
                            <div class="story_title">
                                Suka Duka                            </div>
							Kadang kami membuat kalian semua marah, benci dan kecewa, tapi mengertilah, kami hanya dua insan yang sedang terlanda asmara, kami hanya belajar untuk mengutamakan pasangan hidup kami daripada kalian tanpa melupakan kalian dan tanpa mengurangi rasa cinta kami pada kalian.                        </div> 
                    </div>
                </div>
								                <div class="story_item">
                    <div class="story_plane">
                        <div class="story_back"></div>
                        <div class="story_txt">
                            <div class="story_title">
                                Keindahan Cinta                            </div>
							Tidak ada manusia yang sempurna di dunia ini, tapi kami akan bersama untuk saling melengkapi merajut hidup yang sempurna. Sungguh indah cinta ini, cinta kami yang ber ujung dipernikahan akan menjadi bukti keseriusan kami dalam menata kehidupan dan masa depan.                        </div> 
                    </div>
                </div>
								                <div class="story_item">
                    <div class="story_plane">
                        <div class="story_back"></div>
                        <div class="story_txt">
                            <div class="story_title">
                                Akhir Untuk Memulai                            </div>
							Semua masa yang telah kami lewati kini akan berakhir, dan kami akan memulai masa yang baru berdua dan slalu bersama untuk selamanya hingga ujung waktu.                        </div> 
                    </div>
                </div>
								                <div class="story_item">
                    <div class="story_plane">
                        <div class="story_back"></div>
                        <div class="story_txt">
                            <div class="story_title">
                                Mohon Doa                            </div>
							Semua ini tak lepas dari doa kedua orang tua dan kalian semua, terimakasih telah senantiasa menghantar kami hingga ke titik ini. Mohon doa restu..                        </div> 
                    </div>
                </div>
												
                 
        </div>
    </section>
	

    <!-- When & Where -->
    <section class="when_where white_txt parallax" id="when_where" style="background-image: url(http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/images/img-bawah.jpg)" data-bottom-top="opacity:0;" data-bottom="opacity:1;">
        <div class="over"></div>
        <div class="container">
               
            <!-- Photocamera Icon -->
            <div class="when_where_container opacity">
            <h2><a class="tbl-akd">Akad Nikah</a>&nbsp;<span></span>&nbsp;&nbsp;<a class="tbl-res">Resepsi</a></h2>
                
				<div class="resepsi">
					<!-- Texts -->
					<div class="pattern1"></div>
					<div class="title1 title1-hp">Resepsi</div>
					<div class="title2 title2-hp">
					Kediaman mempelai wanita. Jl. Damai Raya 31 Petukangan, Jakarta Selatan<br /><a href="#google-map"><i class="flaticon-map35"></i></a> </div>

					<div class="pattern2"></div>
					<div class="title1 title1-hp">11:00 - 13:00</div>
					<div class="title5">WIB</div>
					<div class="title5-b">Minggu, 23 Juli 2017</div>
					<div class="pattern3"></div>
				</div>
				<div class="akad">
					<!-- Texts -->
					<div class="pattern1"></div>
					<div class="title1 title1-hp">Akad Nikah</div>
					<div class="title2 title2-hp">Kediaman mempelai wanita. Jl. Damai Raya 31 Petukangan, Jakarta Selatan<br /><i class="flaticon-map35"></i> </div>

					<div class="pattern2"></div>
					<div class="title1 title1-hp">10:00 - 11:00</div>
					<div class="title5">WIB</div>
					<div class="title5-b">Minggu, 23 Juli 2017</div>
					<div class="pattern3"></div>
				</div>	
				
            </div>
        </div>		
    </section>
    <!-- When & Where End -->

    <!-- RSVP -->
	    <section class="rsvp" id="rsvp">
        <div class="container">
            <h2><span>Konfirmasi</span> Kehadiran</h2>

            <div id="envelope" data-100-top="@class:active" data-200-bottom="@class: ">
                <div class="envelope_front"><div class="env_top_top"></div></div>
                <div class="envelope_back"><div class="env_top"></div></div>

                <div class="paper">
       
                    <!-- End Date of Reservation -->
                    <div class="paper_title">Konfirmasi Kehadiran</div>
                    
                    <!-- Form -->
                    <div id="div_block_1">
						<div class="row">
						<div role="form" class="wpcf7" id="wpcf7-f585-o4" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/undangan/great-day-rama-dan-shinta/?nama=Nabila+Astarina#wpcf7-f585-o4" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="585" />
<input type="hidden" name="_wpcf7_version" value="4.4.2" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f585-o4" />
<input type="hidden" name="_wpnonce" value="499dc034c0" />
</div>
<div class="col-md-6 col-sm-6 col-xs-12"><span class="wpcf7-form-control-wrap nama"><input type="text" name="nama" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nama Anda" /></span></div>
<div class="col-md-6 col-sm-6 col-xs-12"><span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email Anda" /></span></div>
<div class="col-md-6 col-sm-6 col-xs-12"><span class="wpcf7-form-control-wrap kehadiran"><input type="text" name="kehadiran" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Hadir/Tidak" /></span></div>
<div class="col-md-6 col-sm-6 col-xs-12"><span class="wpcf7-form-control-wrap ucapan"><input type="text" name="ucapan" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Ucapan" /></span></div>
<p><input type="submit" value="Kirim" class="wpcf7-form-control wpcf7-submit btn-hadir" /></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
						</div>
                        <!-- Form Additional text -->
                        <p>Konfirmasi kehadiran anda akan di kirim langsung ke email kami.</p>
                    </div>
                </div>
                
            </div>
        </div>
    </section> 
		
    <!-- RSVP End -->
	

    <!-- fbkomen -->
    <section id="fbkomentar">
        <div class="container">
            <h2><span>Buku</span> Tamu</h2>

            <div>
				<div class="fb-comments" data-href="http://envita.net/undangan/great-day-rama-dan-shinta/" data-numposts="5"></div>                
            </div>
        </div>
    </section> 
    <!-- fbkomen End -->
	

    <!-- Gallery -->
	    <section class="gallery" id="gallery">
        <h2><span>Galeri</span> Foto</h2>    
        <div class="gallery_wrapper">

			<div class="gallery_item"><a href="http://envita.net/wp-content/uploads/2017/03/2.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/03/2-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="2" /></a></div><div class="gallery_item"><a href="http://envita.net/wp-content/uploads/2017/03/3.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/03/3-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="3" /></a></div><div class="gallery_item"><a href="http://envita.net/wp-content/uploads/2017/03/6.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/03/6-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="6" /></a></div><div class="gallery_item"><a href="http://envita.net/wp-content/uploads/2017/03/8.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/03/8-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="8" /></a></div><div class="gallery_item"><a href="http://envita.net/wp-content/uploads/2017/03/7.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/03/7-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="7" /></a></div><div class="gallery_item"><a href="http://envita.net/wp-content/uploads/2017/03/5.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/03/5-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="5" /></a></div>			
			
        </div>
    </section>
		
    <!-- Gallery End -->
	
    <div id="google-map" class="gmap"></div>
    <style>
        .buka-gmap {
            width: 100%;
            background: #eee;
            padding: 10px;
            padding-top:5px;
            font:22px Roboto Condensed!important;
            margin:0;
            display: block;
            text-align: center;
        }
        .buka-gmap a:active {
            text-decoration:none;
            color: #c96897;
        }
        .buka-gmap a:focus {
            text-decoration:none;
            color: #c96897;
        }        
        .buka-gmap a:hover {
            text-decoration:none;
            color: #c96897;
        }
    </style>
    <div class="buka-gmap"><a href="http://maps.google.com/maps?q=-6.248503, 106.754860" target="_blank"> KLIK UNTUK MEMBUKA PETA DI APP GOOGLE MAP </a></div>

  <script type="text/javascript">
      function initMap() { 
        var envita = new google.maps.LatLng(-6.248503, 106.754860);
        var map = new google.maps.Map(document.getElementById('google-map'), {
          zoom: 16,
          center: envita,
        oomControl: true,
        navigationControl: true,
        scrollwheel: false,
        styles: [
          {
          "featureType":"all",
          "elementType":"all",
            "stylers":[
              { "saturation":"-70" }
            ]
        }]
        });
        var marker = new google.maps.Marker({
          position: envita,
          map: map,
          icon: "http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/images/map_pin.png"
        });
      }
  </script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6J5-_NS1XuofKOPArJFF6bna9TWv8M48&callback=initMap"
        async defer></script>
		
    <!-- Footer -->
    <section class="footer white_txt parallax" id="footer" style="background-image: url(http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/images/img-bawah.jpg)">
        <div class="over"></div>
        <div class="container">

             <!-- Thanks Text -->
            <div class="thanks">Terimakasih Telah Mengunjungi Web Undangan Kami</div>
            <div class="footer_txt">

               


                <!-- Social Buttons -->
                <div class="footer_social">
                    <a href="http://twitter.com/EnvitaOfficial" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="http://facebook.com/envita.net" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="http://instagram.com/envita.official" target="_blank"><i class="fa fa-instagram"></i></a>
                </div>
                
                            <div class="title1">Shinta & Rama</div>
                <!-- Copyrights -->
                <div class="copyrights">&copy; 2019 Shinta & Rama - Provided by <a href="http://envita.net/" target="_blank">Envita.net</a></div>
                
            </div>
        </div>

         

    </section> 

		
    <!-- JQuery -->
	<script src="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/js/jquery-1.11.0.min.js"></script> 
    <!-- CountDown JS -->
    <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/js/jquery.plugin.min.js"></script> 
	<script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/js/jquery.countdown.min.js"></script>

 	<!-- ColorBox JS -->
    <script src="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/js/jquery.colorbox-min.js"></script>
    <!-- OWL Carousel JS -->
	<script src="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/js/owl.carousel.min.js"></script>
    <!-- Bootstrap JS -->
	<script src="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/js/bootstrap.min.js"></script>

    <!-- ScrollR JS -->
    <script src="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/js/skrollr.min.js"></script>
    

    
    <!-- PrefixFree -->
    <script src="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/js/prefixfree.min.js"></script>
    
    <!-- FireFly JS -->
    <script src="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/js/jquery.firefly-0.3-min.js"></script>
    <!-- Theme JS -->
    <script src="http://envita.net/wp-content/themes/invitus%20gongkow/temp-undangan/great-day/js/script.js"></script>
	<script src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/js/jquery.swipebox.js"></script>
	<script src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/js/swipebox-config.js"></script>	
	<script>
			/*CountDown*/
		$('.married_coundown').countdown({until: new Date(2017, 6, 23)});
	</script>


    
 
</div>

</body>
</html>

<!-- Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/

 Served from: envita.net @ 2019-01-23 11:40:11 by W3 Total Cache -->