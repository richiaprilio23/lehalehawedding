
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Pernikahan Denada &amp; Sumanto</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />  
	<meta name="description" content="Website resmi undangan pernikahan Denada &amp; Sumanto - Minggu, 30 Desember 2018" />
	<meta name="keywords" content="website, pernikahan, online" />
    <!-- Dibawah ini font Harus Ada -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Arima+Madurai|Bangers|Caveat|Cinzel|Dancing+Script|Frank+Ruhl+Libre|Julius+Sans+One|Parisienne|Patua+One|Sacramento" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Rubik|Open+Sans|Frank+Ruhl+Libre|Montserrat|Muli|Roboto|Roboto+Slab|Varela+Round" rel="stylesheet">
    <!-- Masukkin Font Tambahan Disini -->
    <link href="https://fonts.googleapis.com/css?family=Rochester" rel="stylesheet" />
    <!-- Batas Font Tambahan -->
	<link rel='dns-prefetch' href='//s.w.org' />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/wanderer.kepelaminan.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='contact-form-7-css'  href='http://wanderer.kepelaminan.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
<link rel='stylesheet' id='style-css'  href='http://wanderer.kepelaminan.com/wp-content/themes/wanderer/style.css?ver=4.9.9' type='text/css' media='all' />
<link rel='stylesheet' id='prettyPhoto-css'  href='http://wanderer.kepelaminan.com/wp-content/themes/wanderer/style/prettyPhoto.css?ver=4.9.9' type='text/css' media='all' />
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var psts_tax = {"taxamo_missmatch":"EU VAT: Your location evidence is not matching. Additional evidence required. If you are travelling in another country or using a VPN, please provide as much information as possible and ensure that it is accurate.","taxamo_imsi_short":"Your IMSI number for your SIM card must be 15 characters long.","taxamo_imsi_invalid":"The IMSI number you provided is invalid for a EU country.","taxamo_overlay_non_eu":"Resident of non-EU country.","taxamo_overlay_detected":"Detected VAT country:","taxamo_overlay_learn_more":"Learn more.","taxamo_overlay_country_set":"VAT country is set to:"};
/* ]]> */
</script>
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-content/plugins/pro-sites/pro-sites-files/js/tax.js?ver=3.5.8'></script>
<link rel='https://api.w.org/' href='http://wanderer.kepelaminan.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://wanderer.kepelaminan.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://wanderer.kepelaminan.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.9" />
		
    <style type="text/css">
    #body {        
                background-image: url(http://wanderer.kepelaminan.com/wp-content/themes/wanderer/images/background.jpg);
            }
    .nama-header {
        color:#d17b7c;
        font-family: 'Julius Sans One',  sans-serif;
        font-size: 50px;
    }
    #text-dan {
        color:#d17b7c;
    }
    .text-judul {
        font-family:'Cinzel', sans-serif;
        font-size:30px;
        color:#a4c9bb;
    }
    #title-story {
        font-family:'Parisienne', sans-serif;
        font-size: 30px;
        color:#d17b7c;
    }
    .text-utama {
         font-family:'Varela Round', sans-serif;
        font-size:16px;
        color:#534d63;
    }
    #text-footer {
        font-family:'', sans-serif;
        font-size:14px;
        color:#969696;
    }
    #tanggal {
            color:#d17b7c;
    }
    .nama-pengantin {
        color:#d17b7c;
        font-family: 'Cinzel',  sans-serif;
        font-size: 34px;
    }
    .judul-akad {
        font-family:'Cinzel', sans-serif;
        font-size:px;
        color:#ffffff;
    }
    </style>
</head>
<body id="body">
       <div id="content-audio">
        <audio controls autoplay>
            <source src="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/10-Cinta-1.m4a" type="audio/mpeg" />
        </audio>
    </div>
		    <div id="wrapper">
        <header id="main-header">
            <div id="header-content">
                <h1 class="clearfix"><em id="header-nama-1" class="nama-header">Denada</em> <span id="text-dan">&amp;</span> <em id="header-nama-2" class="nama-header">Sumanto</em></h1>
            </div>
        </header>
        <div class="grid">
            <div class="grid-sizer"></div>
			            <div id="content-slideshow" class="clearfix grid-item full">
                <div class="wrapper-grid clearfix">
				                    <div id="slideshow">
                        <ul class="slides">
                             <li><img src="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/img-1.jpg" alt="" /></li>							 <li><img src="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/img-2.jpg" alt="" /></li>					 
							 <li><img src="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/img-3.jpg" alt="" /></li>					 
		                     <li><img src="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/img-4.jpg" alt="" /></li>	
                        </ul>
                    </div>
                    			                 </div>
            </div>
			            <div class="grid-item tanggal-nikah">
                <div class="wrapper-grid clearfix">
                    <div id="tanggal">Minggu, 30 Desember 2018</div>
                    					<div id="content-counter">
						                        <div id="countdown">
                        </div>
                                            </div>
					                </div>
            </div>
			            <div id="content-story" class="clearfix grid-item">
                <div class="wrapper-grid clearfix">
                    <h2 id="title-story">Kisah Pertemuan Kami</h2>
					                    <figure id="foto-wanita"><img src="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/img-6.jpg" alt="Denada" /></figure>
										                    <figure id="foto-pria"><img src="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/img-5.jpg" alt="Sumanto" /></figure>
					                    <div class="wrath-frame"></div>
                    <article class="text-utama" id="text-story">
                        <p>Semua bermula ditahun 2007, ketika Dedi menjadi salah satu mahasiswa dalam kelompok KKN kami. Kami bertemu, dan dalam sekejap saja semuanya menjadi terasa lebih indah.</p>
<p>5 tahun kemudian ditahun 2012, Dedi resmi melamar saya, dan 5 tahun kemudian, disinilah kami, siap untuk melanjutkan kejenjang yang lebih tinggi.</p>
                             
                    </article> 
                </div>
            </div>
						            <div id="content-gallery" class="clearfix grid-item">
                <div class="wrapper-grid clearfix">
                    <h2 id="judul-album" class="text-judul">Photo Story</h2>
                    <ul id="list-gallery" class="clearfix">
                                               <li>
                          <a href="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/gal-1.jpg" data-rel="prettyPhoto[1]"><img src="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/gal-1-640x480.jpg" alt="" /></a>
                      </li>
                                                   <li>
                         <a href="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/gal-2.jpg" data-rel="prettyPhoto[1]"><img src="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/gal-2-640x480.jpg" alt="" /></a>
                     </li>
                                                   <li>
                         <a href="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/gal-3.jpg" data-rel="prettyPhoto[1]"><img src="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/gal-3-640x480.jpg" alt="" /></a>
                     </li>
                                                   <li>
                         <a href="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/gal-4.jpg" data-rel="prettyPhoto[1]"><img src="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/gal-4-640x480.jpg" alt="" /></a>
                     </li>
                                                   <li>
                        <a href="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/gal-5.jpg" data-rel="prettyPhoto[1]"><img src="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/gal-5-640x480.jpg" alt="" /></a>
                     </li>
                                                   <li>
                        <a href="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/gal-6.jpg" data-rel="prettyPhoto[1]"><img src="http://wanderer.kepelaminan.com/wp-content/uploads/sites/9/2017/03/gal-6-640x480.jpg" alt="" /></a>
                     </li>
                          
                    </ul>
                </div>
            </div>
			            <div id="content-utama" class="clearfix grid-item">
                <div class="wrapper-grid clearfix">
                    <p id="text-awal" class="text-utama">Maha suci Allah yang telah menciptakan mahluk-Nya berpasang-pasangan, Ya Allah perkenankanlah putra-putri kami:</p>
                    <div id="text-pengantin-utama" class="clearfix">
                        <h2 id="nama-pengantin-1" class="nama-pengantin">Denada Sumarti</h2>
                        <h3 id="text-penghubung">dan</h3>
                        <h2 id="nama-pengantin-2" class="nama-pengantin">Sumanto Johan</h2>
                    </div>
                    <p id="text-tengah" class="text-utama">Untuk mengikuti Sunnah Rasul-Mu dalam membentuk rumah tangga yang sakinah mawaddah dan warrahmah</p>
					                    <div id="content-akad" class="clearfix">
                        <div class="lokasi-nikah">
                            <h3 id="akad-judul" class="judul-akad">Akad Nikah</h3>
                        </div>
                         <p id="akad-tanggal" class="text-utama">26 Desember 2017, 17:00 WIB</p>
						 <p id="akad-lokasi" class="text-utama">Masjid Al-Itiqad</p>
			             <p id="akad-alamat" class="text-utama">Kampung Sleman Selatan, Jl. Surowati 4 No.25, Bandung</p>
                    </div>
										                    <div id="content-resepsi" class="clearfix">
                        <div class="lokasi-nikah">
                            <h3 id="resepsi-judul" class="judul-akad">Resepsi</h3>
                        </div>
                          <p id="resepsi-tanggal" class="text-utama">27 Desember 2017, 11:00 WIB</p>
						  <p id="resepsi-lokasi" class="text-utama">Gedung Serbaguna Lurah</p>
			              <p id="resepsi-alamat" class="text-utama">Gedung Serbaguna Kelurahan Sleman, Bandung Selatan (Depan Bioskop 21)</p>
                    </div>
					                    <div class="clearfix"></div>
					                    <div id="content-maps" class="clearfix">
											<a href="http://maps.apple.com/maps?q=-7.770155,110.377708" target="_blank" class="button"><span id="text-button-lokasi">Peta Lokasi</span></a>
										</div>
					                </div>
            </div>
			             <div id="content-kitab" class="clearfix grid-item">
                <div class="wrapper-grid">
                    <p id="text-kitab">“Dan diantara tanda – tanda kekuasaan-Nya ialah cipataan-Nya untukmu pasangan hidup dari jenismu sendiri, supaya kamu mendapatkan ketenangan hati dan dijadikan-Nya kasih sayang diatara kamu. Sesungguhnya yang demikian menjadi tanda- tanda kebesaran-Nya bagi orang – orang yang berfikir” - QS. Ar Rum : 21</p>
                </div>
            </div>
						            <div id="content-keluarga" class="clearfix grid-item">
                <div class="wrapper-grid clearfix">
                    <h3 id="judul-keluarga" class="text-judul">Kami yang Berbahagia</h3>
                    <div id="keluarga-1">
                         <p id="ayah-1" class="text-utama">Roberto (alm)</p>
		                 <p id="ibu-1" class="text-utama">Rubiyanti</p>
                    </div>
                    <div id="keluarga-2">
                        <p id="ayah-2" class="text-utama">Sumartono</p>
						<p id="ibu-2" class="text-utama">Susilawati</p>
                    </div>
                </div>
            </div>
						            <div id="content-footer" class="clearfix grid-item">
                <div class="wrapper-grid clearfix">
                    <p id="text-footer" class="catatan-footer">Dengan tidak mengurangi rasa hormat, kami tidak menerima tamu dirumah</p>
                </div>
            </div>
        </div>
    </div>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/wanderer.kepelaminan.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.3'></script>
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-content/themes/wanderer/script/jquery.flexslider.js?ver=1'></script>
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-content/themes/wanderer/script/jquery.plugin.js?ver=1'></script>
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-content/themes/wanderer/script/jquery.countdown.js?ver=1'></script>
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-content/themes/wanderer/script/jquery.countdown-id.js?ver=1'></script>
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-content/themes/wanderer/script/jquery.prettyPhoto.js?ver=1'></script>
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-content/themes/wanderer/script/jquery.validate.js?ver=1'></script>
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-content/themes/wanderer/script/theme-customizer.js?ver=1'></script>
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-includes/js/imagesloaded.min.js?ver=3.2.0'></script>
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-includes/js/masonry.min.js?ver=3.3.2'></script>
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-content/themes/wanderer/script/imagesLoaded.js?ver=1'></script>
<script type='text/javascript' src='http://wanderer.kepelaminan.com/wp-includes/js/wp-embed.min.js?ver=4.9.9'></script>
</body>
</html>
