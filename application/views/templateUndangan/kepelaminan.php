<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Pernikahan Renata &amp; Kartono</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />  
	<meta name="description" content="Website resmi undangan pernikahan Renata &amp; Kartono - Senin, 31 Desember 2018" />
	<meta name="keywords" content="website, pernikahan, online" />
	<link rel='dns-prefetch' href='//s.w.org' />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/chopin.kepelaminan.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='contact-form-7-css'  href='http://chopin.kepelaminan.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
<link rel='stylesheet' id='style-css'  href='http://chopin.kepelaminan.com/wp-content/themes/chopin/style.css?ver=4.9.9' type='text/css' media='all' />
<link rel='stylesheet' id='prettyPhoto-css'  href='http://chopin.kepelaminan.com/wp-content/themes/chopin/style/prettyPhoto.css?ver=4.9.9' type='text/css' media='all' />
<script type='text/javascript' src='http://chopin.kepelaminan.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://chopin.kepelaminan.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var psts_tax = {"taxamo_missmatch":"EU VAT: Your location evidence is not matching. Additional evidence required. If you are travelling in another country or using a VPN, please provide as much information as possible and ensure that it is accurate.","taxamo_imsi_short":"Your IMSI number for your SIM card must be 15 characters long.","taxamo_imsi_invalid":"The IMSI number you provided is invalid for a EU country.","taxamo_overlay_non_eu":"Resident of non-EU country.","taxamo_overlay_detected":"Detected VAT country:","taxamo_overlay_learn_more":"Learn more.","taxamo_overlay_country_set":"VAT country is set to:"};
/* ]]> */
</script>
<script type='text/javascript' src='http://chopin.kepelaminan.com/wp-content/plugins/pro-sites/pro-sites-files/js/tax.js?ver=3.5.8'></script>
<link rel='https://api.w.org/' href='http://chopin.kepelaminan.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://chopin.kepelaminan.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://chopin.kepelaminan.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.9" />
	   <style type="text/css">

    #body {        
                background-image: url(http://chopin.kepelaminan.com/wp-content/themes/chopin/images/background.png);
            }
    .nama-header {
        color:#ffffff;
        font-family: 'cinzel',  sans-serif;
        font-size: 44px;
    }
    #text-dan {
        color:#ffffff;
    }
	.text-judul {
		font-family:'Patua One', sans-serif;
        font-size:32px;
        color:#dc5b81;
	}
    #title-story {
        font-family:'Patua One', sans-serif;
        font-size: 30px;
        color:#dc5b81;
    }
    .text-utama {
        font-family:'Rubik', sans-serif;
        font-size:14px;
        color:#333333;
    }
	 #text-footer {
        font-family:'', sans-serif;
        font-size:14px;
        color:#a5a5a5;
    }
    #tanggal {
		    color:#ffffff;
	}
	video , iframe{
		width:100%;
	}
 .nama-pengantin {
        color:#dc5b81;
        font-family: 'Frank Ruhl Libre',  sans-serif;
        font-size: 40px;
    }
	.judul-akad {
		font-family:'Patua One', sans-serif;
        font-size:22px;
        color:#ffffff;
	}
    </style>
    <!-- Dibawah ini font Harus Ada -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Arima+Madurai|Bangers|Caveat|Cinzel|Dancing+Script|Frank+Ruhl+Libre|Julius+Sans+One|Parisienne|Patua+One|Sacramento" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Rubik|Open+Sans|Frank+Ruhl+Libre|Montserrat|Muli|Roboto|Roboto+Slab|Varela+Round" rel="stylesheet">
    <!-- Masukkin Font Tambahan Disini -->
    <link href="https://fonts.googleapis.com/css?family=Rochester" rel="stylesheet" />
    <!-- Batas Font Tambahan -->
	
	
    
</head>
<body id="body">
     <header id="main-header">
	        <div id="content-slideshow" class="clearfix">
						 				 <div id="wp-custom-header" class="wp-custom-header">
					 						<video autoplay loop>
						<source type="video/ogg" src="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/Couple_In_Field_No_Filter.mp4">
						<source type="video/mp4" src="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/Couple_In_Field_No_Filter.mp4">
						Your browser does not support the video tag.
						</video>
					                 </div>
						<div id="header-content">
				 <h1 class="clearfix"><em id="header-nama-1" class="nama-header">Renata</em> <span id="text-dan">&amp;</span> <em id="header-nama-2" class="nama-header">Kartono</em></h1>
                <div id="tanggal">Senin, 31 Desember 2018</div>
            </div>
        </div>
	    </header>
        <div id="content-counter">
	        <div id="countdown">
        </div>
		    </div>
	    <div id="wrapper">
	          <div id="content-story" class="clearfix">
            <h2 id="title-story">Setelah 7 tahun penantian</h2>
			             <figure id="foto-wanita"><img src="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/img-6.jpg" alt="Renata" /></figure>
						             <figure id="foto-pria"><img src="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/img-5.jpg" alt="Kartono" /></figure>
			            
            <div id="heart"></div>
            <article class="text-utama" id="text-story">
                <p>Semua bermula ditahun 2007, ketika Dedi menjadi salah satu mahasiswa dalam kelompok KKN kami. Kami bertemu, dan dalam sekejap saja semuanya menjadi terasa lebih indah.</p>
<p>5 tahun kemudian ditahun 2012, Dedi resmi melamar saya, dan 5 tahun kemudian, disinilah kami, siap untuk melanjutkan kejenjang yang lebih tinggi.</p>
                                  
            </article> 
        </div>
	  	  	        <div id="content-gallery">
            <span class="corner-left"></span>
            <span class="corner-right"></span>
            <h2 id="judul-album" class="text-judul">Bagaimana kami bisa sampai disini</h2>
            <ul id="list-gallery" class="clearfix">
                                       <li>
                          <a href="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/gal-6.jpg" data-rel="prettyPhoto[1]"><img src="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/gal-6-640x480.jpg" alt="" /></a>
                      </li>
                                                   <li>
                         <a href="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/gal-5.jpg" data-rel="prettyPhoto[1]"><img src="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/gal-5-640x480.jpg" alt="" /></a>
                     </li>
                                                   <li>
                         <a href="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/gal-4.jpg" data-rel="prettyPhoto[1]"><img src="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/gal-4-640x480.jpg" alt="" /></a>
                     </li>
                                                   <li>
                         <a href="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/gal-3.jpg" data-rel="prettyPhoto[1]"><img src="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/gal-3-640x480.jpg" alt="" /></a>
                     </li>
                                                   <li>
                        <a href="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/gal-2.jpg" data-rel="prettyPhoto[1]"><img src="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/gal-2-640x480.jpg" alt="" /></a>
                     </li>
                                                   <li>
                        <a href="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/gal-1.jpg" data-rel="prettyPhoto[1]"><img src="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/gal-1-640x480.jpg" alt="" /></a>
                     </li>
                     
            </ul>
        </div>
		        <div id="content-utama" class="clearfix">
            <p id="text-awal" class="text-utama">Akhirnya kami berdua akan melanjutkan jenjang hidup yang lebih tinggi dengan melangsungkan pernikahan</p>
            <div id="text-pengantin-utama" class="clearfix">
                <h2 id="nama-pengantin-1" class="nama-pengantin">Sumanto S.Sn</h2>
                <h3 id="text-penghubung">dengan</h3>
                <h2 id="nama-pengantin-2" class="nama-pengantin">Sumanti Amd D</h2>
            </div>
            <p id="text-tengah" class="text-utama">Kami tunggu kehadirannya di acara resepsi pernikahan kami, dengan tidak mengurangi rasa hormat kami tidak menerima tamu dirumah pada hari berlangsungnya resepsi pernikahan kami.</p>
			            <div id="content-akad" class="clearfix">
                <span class="corner-right"></span>
                <h3 id="akad-judul" class="judul-akad">Akad Nikah</h3>
                 <p id="akad-tanggal" class="text-utama">22 Desember 2018 - 13:40 WIB</p>
		         <p id="akad-lokasi" class="text-utama">Mesjid Isiqlal</p>
	             <p id="akad-alamat" class="text-utama">Jl. Trunjono Tulungagung, No.145 A, Yogyakarta, Indonesia</p>
            </div>
						            <div id="content-resepsi" class="clearfix">
                <span class="corner-right"></span>
                <h3 id="akad-judul" class="judul-akad">Resepsi</h3>
				<p id="resepsi-tanggal" class="text-utama">22 Desember 2018 Jam 18.00 WIB</p>
	            <p id="resepsi-lokasi" class="text-utama">Istana Negara</p>
		        <p id="resepsi-alamat" class="text-utama">Jl. Trunjono Tulungagung, No.145 A, Yogyakarta, Indonesia</p>
            </div>
			            <div class="clearfix"></div>
			             <div id="content-maps" class="clearfix">
								
				<a href="http://chopin.kepelaminan.com/wp-content/uploads/sites/13/2017/05/gal-1.jpg" data-rel="prettyPhoto" class="button"><span id="text-button-lokasi">Peta Lokasi</span></a>
		    			</div>
						            <div id="content-kitab" class="clearfix">
                <p id="text-kitab">“Dan diantara tanda – tanda kekuasaan-Nya ialah cipataan-Nya untukmu pasangan hidup dari jenismu sendiri, supaya kamu mendapatkan ketenangan hati dan dijadikan-Nya kasih sayang diatara kamu. Sesungguhnya yang demikian menjadi tanda- tanda kebesaran-Nya bagi orang – orang yang berfikir” - QS. Ar Rum : 21</p>
            </div>
						            <div id="content-keluarga" class="clearfix">
                <h3 id="judul-keluarga" class="text-judul">Kami yang berbahagia</h3>
                <div id="keluarga-1">
                    <p id="ayah-1" class="text-utama">Bapak John Sumanto</p>
	                <p id="ibu-1" class="text-utama">Ibu Debby</p>
                </div>
                <div id="keluarga-2">
                     <p id="ayah-2" class="text-utama">Bapak Jack Brown</p>
	                 <p id="ibu-2" class="text-utama">Ibu Sumirah</p>
                </div>
            </div>
			        </div>
		        <div id="content-komentar" class="clearfix">
            <h3 id="judul-komentar" class="text-judul">Kesan & Pesan</h3>
            <div id="komentar">
                <div id="comments">
                    
			<div id="comments">


		<ul id="list-comments">
			<li class="comment byuser comment-author-secureadmin bypostauthor even thread-even depth-1 clearfix" id="li-comment-2">
	<img alt='' src='http://0.gravatar.com/avatar/019157a96cfa61133b6c0a2850cdc8b0?s=80&#038;d=mm&#038;r=g' srcset='http://0.gravatar.com/avatar/019157a96cfa61133b6c0a2850cdc8b0?s=160&#038;d=mm&#038;r=g 2x' class='avatar avatar-80 photo' height='80' width='80' />	<article id="comment-2"  class="comment byuser comment-author-secureadmin bypostauthor odd alt thread-odd thread-alt depth-1 clearfix">
		<header class="clearfix">
			<h3><a href='http://kepelaminan.com' rel='external nofollow' class='url'>kepelaminan.com</a></h3><time>8 Mei 2017 6:45 am</time>
		</header>
						<div class="comment-wrapper text-utama">

			<p>Selamat ya mas bro, semoga langgeng dan cepat dapat momongan</p>
			</div>
		<a rel='nofollow' class='comment-reply-link' href='http://chopin.kepelaminan.com/2017/05/05/halo-dunia/?replytocom=2#respond' onclick='return addComment.moveForm( "comment-2", "2", "respond", "1" )' aria-label='Balasan untuk kepelaminan.com'>Balas</a>	</article><!-- #comment-##  -->
	</li><!-- #comment-## -->
	</ul>
														</div>
                </div>
            </div><!-- #komentar -->
        </div>
		        <footer id="main-footer">
            <div id="content-footer" class="clearfix">
                <p id="text-footer" class="catatan-footer">Terimakasih telah mengunjungi website kami, kami tunggu kedatangannya.</p>
            </div>
        </footer>
    </div>
	<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/chopin.kepelaminan.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script type='text/javascript' src='http://chopin.kepelaminan.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.3'></script>
<script type='text/javascript' src='http://chopin.kepelaminan.com/wp-content/themes/chopin/script/jquery.flexslider.js?ver=1'></script>
<script type='text/javascript' src='http://chopin.kepelaminan.com/wp-content/themes/chopin/script/jquery.plugin.js?ver=1'></script>
<script type='text/javascript' src='http://chopin.kepelaminan.com/wp-content/themes/chopin/script/jquery.countdown.js?ver=1'></script>
<script type='text/javascript' src='http://chopin.kepelaminan.com/wp-content/themes/chopin/script/jquery.countdown-id.js?ver=1'></script>
<script type='text/javascript' src='http://chopin.kepelaminan.com/wp-content/themes/chopin/script/jquery.prettyPhoto.js?ver=1'></script>
<script type='text/javascript' src='http://chopin.kepelaminan.com/wp-content/themes/chopin/script/jquery.validate.js?ver=1'></script>
<script type='text/javascript' src='http://chopin.kepelaminan.com/wp-content/themes/chopin/script/theme-customizer.js?ver=1'></script>
<script type='text/javascript' src='http://chopin.kepelaminan.com/wp-includes/js/wp-embed.min.js?ver=4.9.9'></script>
</body>
</html>
