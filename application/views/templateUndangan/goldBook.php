
<html lang="id-ID" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Envi Clean: Naya dan Raden - Undangan Pernikahan Online</title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="shortcut icon" href=""/>
    <link rel="pingback" href="http://envita.net/xmlrpc.php">
	<link href="http://envita.net/wp-content/themes/invitus%20gongkow/includes/css/undangan/enviclean.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="http://envita.net/wp-content/themes/invitus%20gongkow/includes/css/swipebox.css" rel="stylesheet" type="text/css" media="screen" />
	<link rel="stylesheet" href="http://envita.net/wp-content/themes/invitus%20gongkow/css/bootstrap.min.css" />
	<script src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/js/jquery.min.js"></script>
	<script src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/js/jquery.swipebox.js"></script>
	<script src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/js/swipebox-config.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6J5-_NS1XuofKOPArJFF6bna9TWv8M48&callback=initMap"></script>
	<script src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/js/bookblock/modernizr.custom.js"></script>
	<script src="http://envita.net/wp-content/themes/invitus%20gongkow/js/jquery.countdown.min.js"></script>
</head>

<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69815216-3', 'auto');
  ga('send', 'pageview');

</script><div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.6&appId=462160143990173";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<audio autoplay loop id="myAudio">
  <source src="http://envita.net/backsound/brian-mcknight-marry-your-daughter.mp3" type="audio/mpeg">
</audio> 
<style>
    #over-lay {
        position:fixed;
        width: 100vw;
        height: 100vh;
        background-color: rgba(0,0,0,0.75);
        border: none;
        color: white;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: block;
        font-size: 16px;
        margin: 0px;
        cursor: pointer;    
        z-index:9999;
        display: none;
    }
    .tapfp {
        text-align:center;
        font-size: 40px;
        line-height: 45px;
    }
@media screen and (max-width: 769px) {
    #over-lay {
        display: -webkit-flex; /* Safari */
        -webkit-align-items: center; /* Safari 7.0+ */
        display: flex;
        align-items: center;
    }    
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$("#over-lay").click(function(){
			$("#over-lay").fadeOut(650);
		});
});
</script>	
<div onclick="playAudio()" id="over-lay">
    <div style="margin:0 auto;">
        <div class="tapfp">TAP UNTUK MELANJUTKAN</div>
        <div style="text-align:center">Gunakan browser Chrome atau Safari agar website tampil sempurna<br>Gunakan tombol arah di bawah utuk membuka halaman selanjutnya</div>
    </div>
</div>
<script>
var x = document.getElementById("myAudio"); 

function playAudio() { 
    x.play(); 
} 

function pauseAudio() { 
    x.pause(); 
} 
</script>
			<div class="container-fluid">
				<div class="bb-custom-wrapper row">
					<div id="clock" class="condon"></div>
					<script>									
					$('#clock').countdown('2017/12/31 09:00:00', function(event) {
					  $(this).html(event.strftime('%D:%H:%M:%S'));
					});	
					</script>
					<div id="bb-bookblock" class="bb-bookblock">
						<div class="bb-item">
							<div class="konten">
								<div class="konten-dalam konten-home">
									<div class="img-home">								
										<div class="nama-home">Naya & Raden</div>
										<div class="tanggal-home">Minggu, 31 Desember 2017</div>
										<img class="col-xs-12" src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/images/enviclean/wedding-invitation.png">
										<div class="special-invite col-xs-12">
											Kepada Yang Terhormat:<br><span>Nabila Astarina</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="bb-item">
							<div class="konten">
								<div class="konten-dalam">
									<div class="ceremony cleft col-md-6 col-xs-12">
										<div class="cdleft">
											<img class="top-el" src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/images/enviclean/element-top.png">
											<p class="pembuka">Maha suci Allah SWT yang telah menciptakan makhluknya berpasang-pasangan, perkenankanlah kami melaksanakan syariat agamamu mengikuti sunnah Rasul-mu, membentuk keluarga Sakinah Mawadah Warahmah.</p>
											<h2>Naya Agustina</h2>
											<div>Putri dari Bpk. Mahmud Dani dan Ibu. Lala (Jakarta Selatan)</div>
											<p class="dengan">dengan</p>
											<h2>Raden Akhsan</h2>
											<div>Putra dari Bpk. Tyo Akhsan dan Ibu. Aisyah Anggraeni (Jakarta Selatan)</div>
											<img class="bot-el" src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/images/enviclean/element-bottom.png">
										</div>
									</div>
									<div class="ceremony cright col-md-6 col-xs-12">
										<div class="cdright">
										<div class="akad-resepsi">
											<p>RESEPSI</p>
											<p>Minggu, 31 Desember 2017<br>
											Jam 11:30 s/d 15:00 (WIB)<br>
											di Kediaman mempelai wanita. Jl. Macan IX Blok G 13 Rt 13/ 02 No. 31 Petukangan, Jakarta Selatan</p>
										</div>
																				<div class="akad-resepsi">
											<p>AKAD NIKAH</p>
											<p>Minggu, 31 Desember 2017<br>
											Jam 09:00 s/d 11:00 (WIB)<br>
											di Kediaman mempelai wanita. Jl. Macan IX Blok G 13 Rt 13/ 02 No. 31 Petukangan, Jakarta Selatan</p>
										</div>
																				</div>
									</div>
								</div>
							</div>
						</div>
												<div class="bb-item">
							<div class="konten">
								<div class="konten-dalam">
									<div class="divlain">
										<h2>Kisah Cinta</h2><br>
										<p class="ceritamu">Berawal dari waktu yang lama, kami mengisi hari bersama, suka duka, canda tawa, berlari dan terjatuh. Mimpi yang mempersatukan kami, cinta yang membuat kami saling mengerti. Kadang kami membuat kalian semua marah, benci dan kecewa, tapi mengertilah, kami hanya dua insan yang sedang terlanda asmara. Kami akan selalu bersama untuk saling melengkapi merajut hidup yang sempurna. Sungguh indah cinta ini, cinta kami yang ber ujung dipernikahan akan menjadi bukti keseriusan kami dalam menata kehidupan dan masa depan. Semua masa yang telah kami lewati kini akan berakhir, dan kami akan memulai masa yang baru berdua dan slalu bersama untuk selamanya hingga ujung waktu. Semua ini tak lepas dari doa kedua orang tua dan kalian semua, terimakasih telah senantiasa menghantar kami hingga ke titik ini. Mohon doa restu..</p>
									</div>
								</div>
							</div>
						</div>
																		<div class="bb-item">
							<div class="konten">
								<div class="konten-dalam">
									<div class="divlain">
									<h2 style="margin-bottom:40px;">Galeri Prewedding</h2>
										<div class="galeri">
											<div class="col-md-4 col-sm-4 col-xs-6 col-saya"><div class="row"><a href="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="contoh-prewed-1" srcset="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1-300x200.jpg 300w, http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1-600x401.jpg 600w, http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1-768x513.jpg 768w, http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1-1024x684.jpg 1024w, http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1-1170x781.jpg 1170w, http://envita.net/wp-content/uploads/2017/02/contoh-prewed-1.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></a></div></div><div class="col-md-4 col-sm-4 col-xs-6 col-saya"><div class="row"><a href="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-2.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-2-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="contoh-prewed-2" /></a></div></div><div class="col-md-4 col-sm-4 col-xs-6 col-saya"><div class="row"><a href="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-3.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-3-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="contoh-prewed-3" /></a></div></div><div class="col-md-4 col-sm-4 col-xs-6 col-saya"><div class="row"><a href="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-4.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-4-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="contoh-prewed-4" srcset="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-4-300x200.jpg 300w, http://envita.net/wp-content/uploads/2017/02/contoh-prewed-4-600x399.jpg 600w, http://envita.net/wp-content/uploads/2017/02/contoh-prewed-4.jpg 640w" sizes="(max-width: 300px) 100vw, 300px" /></a></div></div><div class="col-md-4 col-sm-4 col-xs-6 col-saya"><div class="row"><a href="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-5.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-5-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="contoh-prewed-5" /></a></div></div><div class="col-md-4 col-sm-4 col-xs-6 col-saya"><div class="row"><a href="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-6.jpg"><img width="300" height="200" src="http://envita.net/wp-content/uploads/2017/02/contoh-prewed-6-300x200.jpg" class="attachment-blog-post-thumb size-blog-post-thumb" alt="contoh-prewed-6" /></a></div></div>	
										</div>
									</div>
								</div>
							</div>
						</div>
												<div class="bb-item">
							<div class="konten">
								<div class="konten-dalam">
									<div class="divlain">
										<h2>Denah Lokasi</h2><br/>
										<a class="butmap2" href="http://maps.google.com/maps?q=-2.9760735,104.77543070000002" target="_blank"> KLIK UNTUK MEMBUKA PETA DI APP GOOGLE MAP </a>
									</div>	
								</div>
							</div>
						</div>
						<div class="bb-item">
							<div class="konten">
								<div class="konten-dalam">
									<div class="divlain">
										<h2>Buku Tamu</h2><br/>
										<div>
											<div class="fb-comments" data-href="http://envita.net/undangan/envi-clean-naya-dan-raden/" data-numposts="5" data-colorscheme="light">
											</div>
										</div>
									</div>
								</div>
																							</div>
						</div>
												<div class="bb-item">
							<div class="konten">
								<div class="konten-dalam">
									<div class="divlain">
										<h2>Konfirmasi Kehadiran</h2><br/>
										<div style="margin-top: 30px">
										<div role="form" class="wpcf7" id="wpcf7-f46-o3" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/undangan/envi-clean-naya-dan-raden/?nama=Nabila+Astarina#wpcf7-f46-o3" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="46" />
<input type="hidden" name="_wpcf7_version" value="4.4.2" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f46-o3" />
<input type="hidden" name="_wpnonce" value="aa4ff06d81" />
</div>
<div class="row" style="margin-bottom:15px;">
<div class="col-md-5 col-sm-12 col-xs-12"><span class="wpcf7-form-control-wrap nama"><input type="text" name="nama" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nama Anda" /></span></div>
<div class="col-md-4 col-sm-6 col-xs-6"><span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email Anda" /></span></div>
<div class="col-md-3 col-sm-6 col-xs-6"><span class="wpcf7-form-control-wrap kehadiran"><select name="kehadiran" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false"><option value="">-- Kehadiran --</option><option value="Hadir">Hadir</option><option value="Tidak Hadir">Tidak Hadir</option></select></span></div>
</div>
<div style="margin-bottom:15px;"><span class="wpcf7-form-control-wrap ucapan"><textarea name="ucapan" cols="40" rows="3" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Ucapan untuk calon mempelai"></textarea></span></div>
<div><input type="submit" value="Konfirmasi Kehadiran" class="wpcf7-form-control wpcf7-submit" /></div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
										</div>
									</div>	
								</div>
								<div class="logo-invitus">
									<a href="http://envita.net/" title="Undangan Pernikahan Online" rel="home" target="_blank">
									<img src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/images/enviclean/logo-envita.png">
									</a>	
								</div>
							</div>	
						</div>	
												
					</div>
					<nav>
						
						<a id="bb-nav-prev" href="#" class="bb-custom-icon bb-custom-icon-arrow-left">Previous</a>
						<a id="bb-nav-next" href="#" class="bb-custom-icon bb-custom-icon-arrow-right">Next</a>
						
					</nav>
				</div>
			</div>
			
										
		<script src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/js/bookblock/jquerypp.custom.js"></script>
		<script src="http://envita.net/wp-content/themes/invitus%20gongkow/includes/js/bookblock/jquery.bookblock.js"></script>
		<script>
			var Page = (function() {
				
				var config = {
						$bookBlock : $( '#bb-bookblock' ),
						$navNext : $( '#bb-nav-next' ),
						$navPrev : $( '#bb-nav-prev' ),
						$navFirst : $( '#bb-nav-first' ),
						$navLast : $( '#bb-nav-last' )
					},
					init = function() {
						config.$bookBlock.bookblock( {
							speed : 800,
							shadowSides : 0.8,
							shadowFlip : 0.7
						} );
						initEvents();
					},
					initEvents = function() {
						
						var $slides = config.$bookBlock.children();

						// add navigation events
						config.$navNext.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'next' );
							return false;
						} );

						config.$navPrev.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'prev' );
							return false;
						} );

						config.$navFirst.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'first' );
							return false;
						} );

						config.$navLast.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'last' );
							return false;
						} );
						
						// add swipe events
						$slides.on( {
							'swipeleft' : function( event ) {
								config.$bookBlock.bookblock( 'next' );
								return false;
							},
							'swiperight' : function( event ) {
								config.$bookBlock.bookblock( 'prev' );
								return false;
							}
						} );

						// add keyboard events
						$( document ).keydown( function(e) {
							var keyCode = e.keyCode || e.which,
								arrow = {
									left : 37,
									up : 38,
									right : 39,
									down : 40
								};

							switch (keyCode) {
								case arrow.left:
									config.$bookBlock.bookblock( 'prev' );
									break;
								case arrow.right:
									config.$bookBlock.bookblock( 'next' );
									break;
							}
						} );
					};

					return { init : init };
			})();
		</script>
		<script>
				Page.init();
		</script>
</body>
</html>

<!-- Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/

 Served from: envita.net @ 2019-01-23 13:28:13 by W3 Total Cache -->