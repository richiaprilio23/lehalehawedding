<footer class="footer minimal bg-dark">
      <div class="container">
        <div class="row">

          <div class="col-md-4 col-md-offset-4">
            
            <div class="footer-logo local-scroll mb-30 wow zoomIn" data-wow-duration="1s" data-wow-delay="0.2s">
              <h2>
                <a href="#home" class="color-white">LehalehaMedia</a>
              </h2>
              <H5 style="color:#fff">UNDANGAN PERNIKAHAN ONLINE<br>(WA/SMS/Telp) </H5>
            </div>

            <div class="socials footer-socials">
              <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
              <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
              <a href="#" target="_blank"><i class="fa fa-google-plus"></i></a>
			  <a href="#" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></i></a>
            </div> <!-- end socials -->
			<!-- <div class="reseller"><a href="http://envita.net/reseller" target="_blank">- Reseller Welcome -</a></div> -->

            <span class="copyright text-center">
              ©2016 Zeal Media  |  Member of <a href="#" target="_blank">Gongkow Creative</a>
            </span>

          </div>

        </div>
      </div>
    </footer> <!-- end footer -->

    <div id="back-to-top">
      <a href="#top"><i class="fa fa-angle-up"></i></a>
    </div>

  </div> <!-- end main-wrapper -->
  
  <!-- jQuery Scripts -->
  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/js/jquery.min.js"></script>
  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/js/gmap3.js"></script>
  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/js/plugins.js"></script>
  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/revolution/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/revolution/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/js/rev-slider.js"></script>
  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/js/scripts.js"></script>


  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/revolution/js/extensions/revolution.extension.actions.min.js"></script>
  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/revolution/js/extensions/revolution.extension.migration.min.js"></script>
  <script type="text/javascript" src="http://envita.net/wp-content/themes/invitus%20gongkow/revolution/js/extensions/revolution.extension.parallax.min.js"></script>


</body>
</html>
<!-- Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/