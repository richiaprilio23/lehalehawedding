<!DOCTYPE html>
<html lang="id-ID" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Undangan Pernikahan Online | Lelaleha Media</title>

	<meta content='Undangan, Undangan Pernikahan, Undangan Online, Undangan Unik, Undangan Murah, Undangan Unik Murah, Undangan Nikah, Undangan Wedding, Kartu Undangan Pernikahan, Undangan Pernikahan Elegan' name='keywords'/>
	<meta content='Undangan pernikahan online unik, murah, dan praktis. Banyak pilihan desain, proses pembuatan sangat mudah dan cepat' name='description'/>
	<meta content='index, follow' name='robots'/>
	<meta content='Aji Setio Aji' name='author'/>
	<meta content='global' name='distribution'/>
	<meta content='general' name='rating'/>
	<meta content='translate' name='google'/>
	<meta content='id' name='language'/>
	<meta content='ID' name='geo.country'/>
	<meta content='Invitus.us' name='copyright'/>	

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="http://envita.net/xmlrpc.php">

  <!-- Google Fonts -->
  <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700%7CPT+Serif:400,700,400italic' rel='stylesheet'>

  <!-- Css -->
  <link rel="stylesheet" href="http://envita.net/wp-content/themes/invitus%20gongkow/css/bootstrap.min.css" />
  <link rel="stylesheet" href="http://envita.net/wp-content/themes/invitus%20gongkow/css/magnific-popup.css" />
  <link rel="stylesheet" href="http://envita.net/wp-content/themes/invitus%20gongkow/css/font-icons.css" />
  <link rel="stylesheet" href="http://envita.net/wp-content/themes/invitus%20gongkow/revolution/css/settings.css" />
  <link rel="stylesheet" href="<?php echo base_url('assets/css/slider.css');?>" />
  <link rel="stylesheet" href="http://envita.net/wp-content/themes/invitus%20gongkow/css/sliders.css">
  <link rel="stylesheet" href="http://envita.net/wp-content/themes/invitus%20gongkow/css/style.css" />
  <link rel="stylesheet" href="http://envita.net/wp-content/themes/invitus%20gongkow/css/responsive.css" />
  <link rel="stylesheet" href="http://envita.net/wp-content/themes/invitus%20gongkow/css/spacings.css" />
  <link rel="stylesheet" href="http://envita.net/wp-content/themes/invitus%20gongkow/css/animate.css" />

  <!-- Favicons -->
  <link rel="shortcut icon" href="http://lembaranbarusby.com/wp-content/uploads/2018/09/cropped-lb-1-32x32.jpg">
  <link rel="apple-touch-icon" href="http://lembaranbarusby.com/wp-content/uploads/2018/09/cropped-lb-1-32x32.jpg">
  <link rel="apple-touch-icon" sizes="72x72" href="http://lembaranbarusby.com/wp-content/uploads/2018/09/cropped-lb-1-32x32.jpg">
  <link rel="apple-touch-icon" sizes="114x114" href="http://lembaranbarusby.com/wp-content/uploads/2018/09/cropped-lb-1-192x192.jpg">

</head>
<body data-spy="scroll" data-offset="60" data-target=".navbar-fixed-top">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69815216-3', 'auto');
  ga('send', 'pageview');
</script>
  <!-- Preloader -->
  <div class="loader-mask">
    <div class="tp-loader spinner1">
    </div>
  </div>

  
  <!-- Navigation -->
  <header class="nav-type-1" id="home">

    <nav class="navbar navbar-fixed-top">
      <div class="navigation-overlay">
        <div class="container relative">

          <form method="get" class="search-wrap" action="http://lembaranbarusby.com">
            <input type="search" class="form-control" placeholder="Type &amp; Hit Enter" value="" name="s" title="Search for:">
          </form>
		  
          <div class="row">

            <div class="navbar-header">
              <!-- Logo -->
              <div class="logo-container">
                <div class="logo-wrap local-scroll">
                  <a href="#home">
                    <img class="logo" src="http://lembaranbarusby.com/wp-content/uploads/2018/09/cropped-logo2.png" alt="logo">
                  </a>
                </div>
              </div>
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div> <!-- end navbar-header -->

            <div class="col-md-9 nav-wrap right">
              <div class="collapse navbar-collapse" id="navbar-collapse">
                
                <ul class="nav navbar-nav navbar-right local-scroll">
                  <li>
                    <a href="#home">Home</a>
                  </li>
                  <li>
                    <a href="#buatundangan">Buat Undangan</a>
                  </li>
                  <li>
                    <a href="#blog">Blog</a>
                  </li>

                  <li>
                    <a href="#tentangkami">Tentang Kami</a>
                  </li>

                  <li>
                    <a href="http://lembaranbarusby.com">Mitra</a>
                  </li>

                  <!-- <li>
                    <a href="#" class="nav-search">
                      <i class="fa fa-search search-trigger"></i>
                      <i class="fa fa-times search-close"></i>
                    </a>
                  </li> 				   -->
                  
                </ul> <!-- end menu -->
              </div> <!-- end collapse -->
            </div> <!-- end col -->
            
          </div> <!-- end row -->
        </div> <!-- end container -->
      </div> <!-- end navigation -->
    </nav> <!-- end navbar -->
</header> <!-- end navigation -->