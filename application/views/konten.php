<div class="container-fluid">
        <div class="row heading">
          <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <h2 class="text-center bottom-line"></h2>
            <p class="subheading text-center"></p>
          </div>
        </div>
        <div class="row heading">
          <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <h2 class="text-center bottom-line">Promo Desain Undangan 2019</h2>
            <p class="subheading text-center">Kami selalu meluncurkan template undangan baru di setiap bulannya</p>
          </div>
        </div>

        <div class="grid-3-col grid-wide grid-gutter grayscale">
		          <div class="work-item web-design print">
            <div class="work-img">
              <img width="480" height="360" src="http://envita.net/wp-content/uploads/2017/04/Envi-Clean-480x360.jpg" 
                class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="Envi Clean" 
                srcset="http://envita.net/wp-content/uploads/2017/04/Envi-Clean-480x360.jpg 480w, 
                http://envita.net/wp-content/uploads/2017/04/Envi-Clean-600x450.jpg 600w, 
                http://envita.net/wp-content/uploads/2017/04/Envi-Clean-768x576.jpg 768w, 
                http://envita.net/wp-content/uploads/2017/04/Envi-Clean.jpg 1024w, 
                http://envita.net/wp-content/uploads/2017/04/Envi-Clean-180x135.jpg 180w" 
                sizes="(max-width: 480px) 100vw, 480px" />			
              <div class="work-overlay">        
                <div class="work-description">
                  <h3>Undangan Envi Clean</h3>
                  <span><span class="amount">Rp350.000,00</span></span>
                </div>
				<div class="tombol">
        <a class="btn btn-md btn-transparent" href="<?php echo base_url('home/enviGold'); ?>" 
        target="_blank">Preview</a>				
        <!-- <a class="btn btn-md btn-transparent" href="">Buat Undangan</a> -->
				</div>
            </div>
				        </div>
                  </div> <!-- end work-item -->


			<div class="work-item web-design print">
        <div class="work-img">
          <img width="480" height="360" src="http://envita.net/wp-content/uploads/2017/04/Envi-Reds-480x360.jpg"
           class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="Envi Reds" 
           srcset="http://envita.net/wp-content/uploads/2017/04/Envi-Reds-480x360.jpg 480w, 
           http://envita.net/wp-content/uploads/2017/04/Envi-Reds-600x450.jpg 600w, 
           http://envita.net/wp-content/uploads/2017/04/Envi-Reds-768x576.jpg 768w, 
           http://envita.net/wp-content/uploads/2017/04/Envi-Reds.jpg 1024w, 
           http://envita.net/wp-content/uploads/2017/04/Envi-Reds-180x135.jpg 180w" 
           sizes="(max-width: 480px) 100vw, 480px" />			
              <div class="work-overlay">        
                <div class="work-description">
                  <h3>Undangan Envi Reds</h3>
                  <span><span class="amount">Rp350.000,00</span></span>
                </div>
				<div class="tombol">
        <a class="btn btn-md btn-transparent" 
        href="<?php echo base_url('home/enviRed')?>" target="_blank">Preview</a>				
        <!-- <a class="btn btn-md btn-transparent" href="http://envita.net/produk/undangan-envi-reds/">Buat Undangan</a> -->
				</div>
            </div>
				        </div>
                  </div> <!-- end work-item -->

		<div class="work-item web-design print">
            <div class="work-img">
        <img width="480" height="360" src="http://envita.net/wp-content/uploads/2017/04/Envi-Black-480x360.jpg" 
        class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="Envi Black" 
        srcset="http://envita.net/wp-content/uploads/2017/04/Envi-Black-480x360.jpg 480w, 
        http://envita.net/wp-content/uploads/2017/04/Envi-Black-600x450.jpg 600w,
        http://envita.net/wp-content/uploads/2017/04/Envi-Black-768x576.jpg 768w, 
        http://envita.net/wp-content/uploads/2017/04/Envi-Black.jpg 1024w, 
        http://envita.net/wp-content/uploads/2017/04/Envi-Black-180x135.jpg 180w" 
        sizes="(max-width: 480px) 100vw, 480px" />			
              <div class="work-overlay">        
                <div class="work-description">
                  <h3>Undangan Envi Black</h3>
                  <span><span class="amount">Rp350.000,00</span></span>
                </div>
				<div class="tombol">
        <a class="btn btn-md btn-transparent" href="<?php echo base_url('home/enviBlack')?>" 
        target="_blank">Preview</a>				
        <!-- <a class="btn btn-md btn-transparent" href="http://envita.net/produk/undangan-envi-black/">Buat Undangan</a> -->
				</div>
            </div>
				            </div>
          </div> <!-- end work-item -->
          
			<div class="work-item web-design print">
        <div class="work-img">
        <img width="480" height="360" src="http://envita.net/wp-content/uploads/2017/01/Envi-Navy-Theme-480x360.jpg" 
        class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="Undangan Pernikahan Online berbasis web" 
        srcset="http://envita.net/wp-content/uploads/2017/01/Envi-Navy-Theme-480x360.jpg 480w, 
        http://envita.net/wp-content/uploads/2017/01/Envi-Navy-Theme-600x450.jpg 600w, 
        http://envita.net/wp-content/uploads/2017/01/Envi-Navy-Theme-768x576.jpg 768w, 
        http://envita.net/wp-content/uploads/2017/01/Envi-Navy-Theme.jpg 1024w, 
        http://envita.net/wp-content/uploads/2017/01/Envi-Navy-Theme-180x135.jpg 180w" sizes="(max-width: 480px) 100vw, 480px" />			
              <div class="work-overlay">        
                <div class="work-description">
                  <h3>Undangan Envi Navy</h3>
                  <span>
                    <span class="amount">Rp350.000,00</span></span>
                </div>
				<div class="tombol">
        <a class="btn btn-md btn-transparent" href="<?php echo base_url('Home/enviBlue')?>" target="_blank">Preview</a>			
        <!-- <a class="btn btn-md btn-transparent" href="http://envita.net/produk/envi-navy/">Buat Undangan</a> -->
				</div>
              </div>
				            </div>
          </div> <!-- end work-item -->
					          <div class="work-item web-design print">
            <div class="work-img">
        <img width="480" height="360" src="http://envita.net/wp-content/uploads/2016/12/Great-Day-fix-480x360.jpg" 
        class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="great-day-fix" 
        srcset="http://envita.net/wp-content/uploads/2016/12/Great-Day-fix-480x360.jpg 480w, 
        http://envita.net/wp-content/uploads/2016/12/Great-Day-fix-600x450.jpg 600w, 
        http://envita.net/wp-content/uploads/2016/12/Great-Day-fix-768x576.jpg 768w, 
        http://envita.net/wp-content/uploads/2016/12/Great-Day-fix.jpg 1024w, 
        http://envita.net/wp-content/uploads/2016/12/Great-Day-fix-180x135.jpg 180w" 
        sizes="(max-width: 480px) 100vw, 480px" />			
              <div class="work-overlay">        
                <div class="work-description">
                  <h3>Undangan Online Great Day</h3>
                  <span>
                    <span class="amount">Rp350.000,00</span></span>
                </div>
				<div class="tombol">
        <a class="btn btn-md btn-transparent" href="<?php echo base_url('/home/shintadanRama')?>" target="_blank">Preview</a>				
        <!-- <a class="btn btn-md btn-transparent" href="http://envita.net/produk/undangan-online-great-day/">Buat Undangan</a> -->
				</div>
              </div>
				            </div>
          </div> <!-- end work-item -->
					          <div class="work-item web-design print">
            <div class="work-img">
        <img width="480" height="360" src="http://envita.net/wp-content/uploads/2016/09/Marroon-Card-480x360.jpg" 
        class="attachment-shop_catalog size-shop_catalog wp-post-image" 
        alt="Undangan Pernikahan Online Unik Marroon Card" 
        srcset="http://envita.net/wp-content/uploads/2016/09/Marroon-Card-480x360.jpg 480w, 
        http://envita.net/wp-content/uploads/2016/09/Marroon-Card-600x450.jpg 600w, 
        http://envita.net/wp-content/uploads/2016/09/Marroon-Card-768x576.jpg 768w, 
        http://envita.net/wp-content/uploads/2016/09/Marroon-Card.jpg 1024w, 
        http://envita.net/wp-content/uploads/2016/09/Marroon-Card-180x135.jpg 180w" 
        sizes="(max-width: 480px) 100vw, 480px" />			
              <div class="work-overlay">        
                <div class="work-description">
                  <h3>Undangan Unik Marroon Card</h3>
                  <span>
                    <span class="amount">Rp300.000,00</span></span>
                </div>
				<div class="tombol">
          <a class="btn btn-md btn-transparent" href="<?php echo base_url('home/simpleCard')?>" target="_blank">Preview</a>				
          <!-- <a class="btn btn-md btn-transparent" href="http://envita.net/produk/undangan-unik-marroon-card/">Buat Undangan</a> -->
				</div>
          </div>
	          </div>
            </div> <!-- end work-item -->
    

