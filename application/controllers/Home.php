<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
    }
    public function index()
    {
        $this->load->view('home');
    }
    public function enviGold()
    {
        $this->load->view('templateUndangan/goldBook');
    }
    public function enviBlue()
    {
        $this->load->view('templateUndangan/blueBook');
    }
    public function enviBlack(){
        $this->load->view('templateUndangan/blackBook');
    }
    public function enviRed(){
        $this->load->view('templateUndangan/redBook');
    }
    public function shintadanRama(){
        $this->load->view('templateUndangan/shintaRama');
    }
    public function simpleCard(){
        $this->load->view('templateUndangan/simpleCard');
    }
}

